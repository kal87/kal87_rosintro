# Modified code from https://github.com/ros-planning/moveit_tutorials/blob/master/doc/move_group_python_interface/scripts/move_group_python_interface_tutorial.py
from __future__ import print_function
from six.moves import input

import sys
import rospy
import moveit_commander

from math import pi

class MoveGroupPythonInterface(object):
    """MoveGroupPythonInterface"""

    def __init__(self):
        super(MoveGroupPythonInterface, self).__init__()

        #initialize `moveit_commander`_ and a `rospy`_ node:
        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node("move_group_python_interface", anonymous=True)

        # Instantiate a `RobotCommander`_ object. Provides information such as the robot's
        # kinematic model and the robot's current joint states
        robot = moveit_commander.RobotCommander()

        # Instantiate a `PlanningSceneInterface`_ object.  This provides a remote interface
        # for getting, setting, and updating the robot's internal understanding of the
        # surrounding world:
        scene = moveit_commander.PlanningSceneInterface()

        # Instantiate a `MoveGroupCommander`_ object.  This object is an interface to a planning group (group of joints).
        group_name = "manipulator"
        move_group = moveit_commander.MoveGroupCommander(group_name)

        # Getting Basic Information
        # get the name of the reference frame for this robot:
        planning_frame = move_group.get_planning_frame()
        print("============ Planning frame: %s" % planning_frame)

        # print the name of the end-effector link for this group:
        eef_link = move_group.get_end_effector_link()
        print("============ End effector link: %s" % eef_link)

        # get a list of all the groups in the robot:
        group_names = robot.get_group_names()
        print("============ Available Planning Groups:", robot.get_group_names())

        # print the entire state of the robot for debugging:
        print("============ Printing robot state")
        print(robot.get_current_state())
        print("")

        # Misc variables
        self.robot = robot
        self.scene = scene
        self.move_group = move_group
        self.planning_frame = planning_frame
        self.eef_link = eef_link
        self.group_names = group_names

    def write_letter(self,j_goals):
        # Copy class variables to local variables
        move_group = self.move_group
        n = len(j_goals)

        for i in range(0,n):
            #first position is to set up where to start the letter
            if i == 0:
                print("Moving to start position...")
            elif i==1:
                print("Drawing letter...")
            # set joint goals for urf joints (in radians)
            joint_goal = move_group.get_current_joint_values()
            joint_goal[0] = j_goals[i][0]*pi/180
            joint_goal[1] = j_goals[i][1]*pi/180
            joint_goal[2] = j_goals[i][2]*pi/180
            joint_goal[3] = j_goals[i][3]*pi/180
            joint_goal[4] = j_goals[i][4]*pi/180
            joint_goal[5] = j_goals[i][5]*pi/180

            #go to joint goal
            move_group.go(joint_goal, wait=True)

        # ensure that there is no residual movement
        move_group.stop()

    def write_initials(self):
        # create an array of joint goals to go to (in degrees because I got the values from MoveIt) for each initial
        k_joint_goals = [[210,-121,-109,48,150,179], [198,-101,-81,-2,162,178], [206,-110,-100,28,154,179],
                   [229, -53, -119, -8, 131, 181], [207, -104, -105, 27, 153, 179], [270, -90, -156, 65, 90, 181]]
        a_joint_goals = [[210,-121,-109,48,150,179],[209,-80,-107,5,151,179],[228,-80,-131,30,132,180],[213,-97,-114,29,147,179],[228,-80,-131,30,132,180],[270, -90, -156, 65, 90, 181]]
        l_joint_goals = [[212,-81,-107,6,148,179],[224,-110,-126,55,136,180],[270, -90, -156, 65, 90, 181]]
        print('The letter K')
        self.write_letter(k_joint_goals)
        print('The letter A')
        self.write_letter(a_joint_goals)
        print('The letter L')
        self.write_letter(l_joint_goals)

def main():
    try:
        print("Let's write the initials KAL!")
        input(
            "============ Press `Enter` to set up the moveit_commander ..."
        )
        ur5_command = MoveGroupPythonInterface()

        input(
            "============ Press `Enter` to execute a movement using a joint state goal ..."
        )
        ur5_command.write_initials()

        print("Initials complete!")
    except rospy.ROSInterruptException:
        return
    except KeyboardInterrupt:
        return


if __name__ == "__main__":
    main()
