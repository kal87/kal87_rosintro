rosservice call /reset
rosservice call /turtle1/set_pen 50 168 90 5 on
rosservice call /turtle1/teleport_absolute 2 2 0
rosservice call /turtle1/set_pen 50 168 90 5 off
rosservice call /spawn 6 3.5 2.4 ""
rosservice call /turtle2/set_pen 168 82 50 5 off
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[0.0, 4.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[4.0, 0.0, 0.0]' '[0.0, 0.0, 4.5]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[0.0, -2.0, 0.0]' '[0.0, 0.0, 0.0]'
rosservice call /turtle2/teleport_absolute 6 2 1.57
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[1.5, 2.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[1.75, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[-1.5, -2.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[1.5, -2.0, 0.0]' '[0.0, 0.0, 0.0]'



