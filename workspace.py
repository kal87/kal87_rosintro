from collections import namedtuple
import math
import matplotlib.pyplot as plt

Point = namedtuple('Point', 'x y')

def dist(p1, p2):
    '''Calculates 2-norm or Euclidean distance between two points'''
    return math.sqrt((p1.x-p2.x)**2 + (p1.y-p2.y)**2)

class RectWorkspace:
    '''Class for a rectangular workspace and rectangular obstacles'''
    def __init__(self, width, height, obs=None):
        self.width = width
        self.height = height
        self.obs = obs
        pass
    def is_collision(self, pt):
        '''Check if a point has a collision with any obstacle'''
        if self.obs:
            for x,y,w,h in self.obs:
                if pt.x > x and pt.x < x+w and pt.y > y and pt.y <y+h:
                    return True
                pass
            pass
        return False
    def plot(self):
        plt.plot([0,0,self.width,self.width,0],[0,self.height,self.height,0,0])
        if self.obs:
            for x,y,w,h in self.obs:
                plt.fill([x,x,x+w,x+w,x],[y,y+h,y+h,y,y],'r')
                pass
            pass
        pass
    pass

